# For History

HISTFILE=~/.zsh/.histfile
HISTSIZE=5000
SAVEHIST=100000


### Customization ###


#eval "$(starship init zsh)"

precmd() {print ""}

PROMPT='%B%F{#94e2d5}%~ 
%(?.%F{#a6e3a1}.%F{#f38ba8})% ❯%f%b '

source ~/.zsh/catppuccin_macchiato-zsh-syntax-highlighting.zsh


# Zsh Plugins
source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
source ~/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh



### Aliases ###



# XSel
alias pbcopy='xsel --input --clipboard'
alias pbpaste='xsel --output --clipboard'


# misc.
alias sudo="sudo "
alias vim="nvim"


# pacman & yay updates
alias update='sudo pacman -Syyu'
alias upall='yay -Syu --noconfirm'


# listing
alias ls='exa -l --color=always --group-directories-first'
alias ll='exa -al --color=always --group-directories-first'
alias la='exa -a --color=always --group-directories-first'
alias lt='exa -aT --color=always --group-directories-first'
alias l.='exa -a | grep "^\."'


# grep
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'


# adding-flags
alias cp='cp -i'
alias df='df -h'
alias free='free -m'


# git
alias addup='git add -u'
alias addall='git add .'
alias commit='git commit -m'
alias fetch='git fetch'
alias pull='git pull origin'
alias push='git push origin'
alias status='git status'


# shutdown and reboot
alias ss='sudo shutdown now'
alias sr='sudo reboot'


# yt-dlp
alias yta-aac='yt-dlp --extract-audio --audio-format aac '
alias yta-best='yt-dlp --extract-audio --audio-format best '
alias yta-flac='yt-dlp --extract-audio --audio-format flac '
alias ytv='yt-dlp -f bestvideo+bestaudio '


# switch between shells
alias tobash='sudo chsh $USER -s /bin/bash && echo "Now log out."'
alias tozsh='sudo chsh $USER -s /bin/zsh && echo "Now log out."'
alias tofish='sudo chsh $USER -s /bin/fish && echo "Now log out."'


# cd
alias ..='cd ..'
alias ...='cd ../..'


# the terminal rickroll
alias rr='curl -s -L https://raw.githubusercontent.com/keroserene/rickrollrc/master/roll.sh | bash'


### Misc. ###

alias Qtile="startx ~/.config/Xorg/.qtile"
alias Xmonad="startx ~/.config/Xorg/.xmonad"


# For AutoComplete

zstyle :compinstall filename '/home/parth/.zshrc'
autoload -Uz compinit
compinit
#eval "$(register-python-argcomplete pipx)"


# VI Mode

bindkey -e
export KEYTIMEOUT=1


# Created by `pipx` on 2024-08-04 16:34:04
#export PATH="$PATH:/home/ps/.local/bin"
