vim.cmd("set number relativenumber")
vim.cmd("set scrolloff=5")
vim.cmd("set expandtab")
vim.cmd("set tabstop=3")
vim.cmd("set softtabstop=3")
vim.cmd("set shiftwidth=3")
vim.g.mapleader = " "
vim.opt.cursorline = true
vim.cmd("nmap <C-n> :Ex<CR>")
vim.cmd("map <A-n> <C-^>")
