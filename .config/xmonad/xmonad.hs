import XMonad
import XMonad.StackSet as W
import XMonad.ManageHook

import XMonad.Util.EZConfig
import XMonad.Util.Loggers
import XMonad.Util.SpawnOnce
import XMonad.Util.Cursor
import XMonad.Util.NamedScratchpad

import qualified XMonad.Util.Hacks as Hacks

import XMonad.Layout.NoBorders

import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.StatusBar
import XMonad.Hooks.StatusBar.PP
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.ManageDocks

import XMonad.Actions.ToggleFullFloat
import XMonad.Actions.CycleWS


main :: IO ()
main = xmonad 
     . toggleFullFloatEwmhFullscreen
     . ewmhFullscreen 
     . ewmh 
     . withEasySB (statusBarProp "xmobar ~/.config/xmobar/xmobarrc" (pure myXmobarPP)) toggleStrutsKey
     $ myConfig
  where
    toggleStrutsKey :: XConfig Layout -> (KeyMask, KeySym)
    toggleStrutsKey XConfig{ modMask = m } = (m, xK_o)


menu = "dmenu_run -b -fn JetBrainsMonoNF -nb '#1e1e2e' -nf '#fff'"

myConfig = def
    { modMask = mod4Mask
    , layoutHook         = myLayout
    , startupHook        = myStartupHook
    , borderWidth        = 2
    , focusedBorderColor = "#bd93f9"
    , normalBorderColor  = "#000000"
    , terminal           = "alacritty --title term"

    , handleEventHook    = handleEventHook def 
                        <> Hacks.trayerPaddingXmobarEventHook
                        <> Hacks.trayerAboveXmobarEventHook

    , manageHook         = myManageHook
                        <> namedScratchpadManageHook scratchpads
    }
  `additionalKeysP`
    [ ("M-b", spawn "firefox")
    , ("M-p", spawn menu)
    , ("M-C-s", unGrab *> spawn "scrot -F img2.png")
    , ("C-<Space>", spawn "dunstctl close")
    , ("M-s h", namedScratchpadAction scratchpads "htop")
    , ("M-s m", namedScratchpadAction scratchpads "spotify")
    , ("M-s t", namedScratchpadAction scratchpads "term")
    , ("M-f", withFocused toggleFullFloat)
    , ("<XF86AudioRaiseVolume>", spawn "amixer -M set Master 5%+")
    , ("<XF86AudioLowerVolume>", spawn "amixer -M set Master 5%-")
    , ("<XF86AudioMute>", spawn "amixer set Master toggle")
    , ("<XF86MonBrightnessUp>", spawn "brillo -q -u 500000 -A 5")
    , ("<XF86MonBrightnessDown>", spawn "brillo -q -u 500000 -U 5")
    , ("M-<Tab>", nextWS)
    , ("M-S-<Tab>", prevWS)
    ]



myStartupHook :: X ()
myStartupHook = do
  setDefaultCursor xC_left_ptr
  spawnOnce "xwallpaper --zoom ~/Pictures/car.jpg"



myManageHook :: ManageHook
myManageHook = composeAll
    [ className =?  "Gimp" --> doFloat
    , isDialog             --> doFloat
    , isInProperty "_NET_WM_WINDOW_TYPE" "_NET_WM_WINDOW_TYPE_SPLASH" --> doFloat
    , className =? "firefox" <&&> resource =? "Toolkit" --> doFloat
    ]


scratchpads = [
      NS "htop" "st -e htop" (title =? "htop") middleFloating ,
      NS "spotify" "spotify-launcher" (className =? "Spotify") bigFloating ,
      NS "term" "alacritty --title ScratchTerm" (title =? "ScratchTerm") bigFloating
      ]
      where
         middleFloating = customFloating $ W.RationalRect (1/6) (1/6) (2/3) (2/3)
         bigFloating = customFloating $ W.RationalRect (1/10) (1/8) (8/10) (6/8)



myLayout = smartBorders $ Tall 1 (3/100) (1/2) ||| Full



myXmobarPP :: PP
myXmobarPP = def
    { ppSep             = lowWhite " | "
    , ppTitleSanitize   = xmobarStrip
    , ppCurrent         = blue . wrap " " "" . xmobarBorder "Bottom" "#8be9fd" 2
    , ppHidden          = white . wrap " " ""
    , ppHiddenNoWindows = lowWhite . wrap " " ""
    , ppUrgent          = red . wrap (yellow "!") (yellow "!")
    , ppOrder           = \[ws, l, _, wins] -> [ws, l, wins]
    , ppExtras          = [logTitles formatFocused formatUnfocused]
    }
  where
    formatFocused   = wrap (white    "[") (white    "]") . magenta . ppWindow
    formatUnfocused = wrap (lowWhite "[") (lowWhite "]") . blue    . ppWindow
    ppWindow :: String -> String
    ppWindow = xmobarRaw . (\w -> if null w then "untitled" else w) . shorten 20

    blue, lowWhite, magenta, red, white, yellow :: String -> String
    magenta  = xmobarColor "#b4b2f2" ""
    blue     = xmobarColor "#89b4fa" ""
    white    = xmobarColor "#cddcf4" ""
    yellow   = xmobarColor "#f9e2af" ""
    red      = xmobarColor "#f38ba8" ""
    lowWhite = xmobarColor "#585b70" ""
