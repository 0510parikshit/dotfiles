Gtk-Theme = catppuccin-gtk-theme-mocha (aur)
     Set-in : Lxappearance

Qt-Theme = Catppuccin-Mocha  (https://github.com/catppuccin/qt5ct) (copy Catppuccin-Mocha.conf from the themes folder in ~/.config/qt5ct/colors/)
     Set-in : Qt5ct as : 
                      Edit "/etc/environment" as : "QT_QPA_PLATFORMTHEME=qt5ct"

Icon-Theme = papirus-icon-theme (pacman)
     Set-in : Lxappearance

Sddm-Theme= sddm-catppuccin-git (aur)
     Set-in : "/usr/lib/sddm/sddm.conf.d/default.conf" as : "Current=catppuccin"

Mouse-Cursor-Theme = capitaine-cursors (pacman)
     Set-in : "/usr/share/icons/default/index.theme" as : "Inherits=capitaine-cursors"
              "~/.config/gtk-3.0/settings.ini" as : "gtk-cursor-theme-name=capitaine-cursors"
